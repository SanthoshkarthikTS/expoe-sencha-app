/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('CRUD.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    store: {
        type: 'personnel',
    },

    onClick: function(grid) {
        Ext.Msg.alert("tesdt")
    },

    onDeleteClick: function(selModel, record, index, options, grid, store) {
        var userGrid = this.getView();
        console.log(Ext.getStore('personnel'))
        var userStore = userGrid.getStore();

        //delete selected rows if selModel is checkboxmodel
        var selectedRows = userGrid.getSelectionModel().getSelection();

        // userStore.remove(selectedRows);
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        Ext.Msg.confirm({
            title: 'Confirm',
            msg: 'Are you sure?',
            buttons: Ext.Msg.OKCANCEL,
            fn: this.onConfirm,
            icon: Ext.MessageBox.QUESTION,
            config: {
                grid: grid,
                action: 'del',
                store: store,
                selectedRows: selectedRows,
                userStore: userStore
            }
        });
    },

    onConfirm: function(btn, text, opt) {
        console.log(opt.config.action)
        if (btn === 'ok') {
            //
            opt.grid.userStore.remove(opt.grid.selectedRows)
                // opt.config.grid.item.remove();
            Ext.Ajax.request({
                url: 'http://localhost:8080/' + opt.config.action + '/' + opt.config.grid.record.data.id,
                useDefaultXhrHeader: false,
                success: function(form, action) {
                    opt.config.store.load({
                        start: 0,
                        limit: 5
                    });
                    console.log(Ext.getCmp("pagingtoolbar"))
                },
                failure: function(form, action) {

                },
                listeners: {
                    doRefresh: function() {
                        this.doLoad(this.cursor);
                    },
                }
            });
        }
    }
});