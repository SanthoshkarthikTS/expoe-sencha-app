/**
 * This view is an example list of people.
 */
Ext.define('CRUD.view.add.AddUser', {
    extend: 'Ext.FormPanel',
    xtype: 'adduser',

    controller: 'add',

    title: 'Add User',
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'Name',
        name: 'name',
    }, {
        fieldLabel: 'Email',
        name: 'email',
        vtype: 'email'
    }, {
        regex: /^([0-9]){7,10}$/,
        fieldLabel: 'Phone',
        name: 'phone',
    }],
    buttons: [{
        text: 'Save',
        formBind: true,
        listeners: {
            click: 'onUserSubmit'
        }
    }],



});