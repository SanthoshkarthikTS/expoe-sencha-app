/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('CRUD.view.add.AddUserController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.add',

    onUserSubmit: function(button, e, options) {
        var formObj = Ext.ComponentQuery.query('adduser')[0];

        var formValues = formObj.getValues();
        //Ext.Msg.alert(formValues);
        Ext.Ajax.request({
                url: 'http://localhost:8080/add?' + Ext.urlEncode(formValues),
                method: 'POST',
                useDefaultXhrHeader: false,
                params: formValues,
                success: function(response) {
                    location.href = 'http://localhost:1841';
                },
                failure: function(form, action) {
                    // location.href = 'http://localhost:1841';
                }
            }) //, // location.href = 'http://localhost:1841';
    }
});