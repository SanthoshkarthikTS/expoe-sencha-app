var express = require('express'),
    app = express(),
    conn = require('./db');

// Add headers
app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:1841');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/list', function(req, res) {
    var sql = "SELECT (SELECT count( * ) FROM heroes) as count, id, name, email, phone FROM heroes limit " + req.query.limit + " offset " + req.query.offset
    query(sql, res, 'list')
        // console.log(sql)
})

app.post('/add', function(req, res) {
    var name = req.query.name,
        email = req.query.email,
        phone = req.query.phone
    if (name != "" && email != "" && phone != "") {
        var sql = "INSERT INTO heroes (name, email, phone) VALUES ('" + name + "','" + email + "','" + phone + "')";
        query(sql, res, 'add')
    }
})

app.post('/edit', function(req, res) {
    var id = req.query.id,
        name = req.query.name,
        email = req.query.email,
        phone = req.query.phone;
    if (id != "" && name != "" && email != "" && phone != "") {
        var sql = "UPDATE heroes SET name='" + name + "',email='" + email + "',phone='" + phone + "'" + " WHERE id=" + id
        query(sql, res, 'edit')
            //  console.log(sql)
    }
})

app.get('/del/:Id', function(req, res) {
    var Id = req.params.Id
    var sql = "DELETE FROM heroes WHERE Id=" + Id;
    //  console.log(sql)
    query(sql, res, 'del')

})

function query(sql, res, option) {
    conn.query(sql, function(err, result) {
        if (err) throw err;
        if (option == 'list') {
            var output = { "total": result[0].count, "items": result }
                // res.jsonp(output)
            res.send(JSON.stringify(output))
            console.log("list")
        } else {
            res.jsonp(result)
        }

        //console.log(res.jsonp(result))
    })
}

app.listen(8080, 'localhost')