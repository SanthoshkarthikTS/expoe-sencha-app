Ext.define('CRUD.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    model: 'CRUD.model.User',

    fields: [
        'name', 'email', 'phone'
    ],

    storeId: 'personnel',
    autoLoad: { offset: 0, limit: 5 },
    autoSync: true,
    pageSize: 5,
    proxy: {
        type: 'ajax', //cross domain calls - json parser
        enablePaging: true,
        url: 'http://localhost:8080/list',
        useDefaultXhrHeader: false,
        startParam: 'offset',
        limitParam: 'limit',
        reader: {
            totalProperty: 'total',
            rootProperty: 'items'
        },
    },
});