### ./sass/var

This folder contains Sass files defining Sass variables corresponding to classes
included in the application's JavaScript code build when using the modern toolkit.
By default, files in this folder are mapped to the application's root namespace,
'CRUD' in the same way as `"CRUD/sass/src"`.