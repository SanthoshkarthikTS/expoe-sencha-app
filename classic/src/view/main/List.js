/**
 * This view is an example list of people.
 */
Ext.define('CRUD.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'home',
    requires: [
        'CRUD.store.Personnel',
        'CRUD.view.main.MainController',
        'Ext.toolbar.Paging',
    ],

    title: 'Heroes',
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],
    layout: 'fit',
    fullscreen: true,
    store: {
        type: 'personnel',
    },
    selModel: {
        pruneRemoved: false
    },
    selType: 'cellmodel',
    columns: [{
            text: 'Name',
            align: 'center',
            dataIndex: 'name',
            sortable: true,
            flex: 1,
            editor: {
                xtype: 'textfield',
                selectOnFocus: true,
                allowBlank: false
            }
        },
        {
            text: 'Email',
            align: 'center',
            dataIndex: 'email',
            sortable: true,
            flex: 1,
            editor: {
                xtype: 'textfield',
                selectOnFocus: true,
                allowBlank: false
            }
        },
        {
            text: 'Phone',
            align: 'center',
            dataIndex: 'phone',
            sortable: true,
            flex: 1,
            editor: {
                xtype: 'textfield',
                selectOnFocus: true,
                allowBlank: false
            }
        },
        {
            text: 'Save',
            align: 'center',
            xtype: 'actioncolumn',
            items: [{
                icon: 'http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/128/Actions-document-edit-icon.png',
                xtype: 'submit',
                handler: function(grid, rowIndex, colIndex, item, e, record) {
                    Ext.Msg.confirm("Confirmation", "Do you want to Save?", function(btnText) {
                        if (btnText === "yes") {
                            Ext.Ajax.request({
                                url: 'http://localhost:8080/edit?id=' + record.data.id + '&name=' + record.data.name + '&email=' + record.data.email + '&phone=' + record.data.phone,
                                method: 'POST', //this is the url where the form gets submitted
                                useDefaultXhrHeader: false,
                                success: function(response) {
                                    store.load()
                                },
                                failure: function(form, action) {
                                    Ext.Msg.alert('Failed', action);
                                }
                            });
                        }
                    });
                }
            }],

        }, {
            text: 'Delete',
            xtype: 'actioncolumn',
            align: 'center',
            items: [{
                icon: 'http://www.freeiconspng.com/uploads/delete-button-png-27.png',
                xtype: 'submit',
                //     handler: function(grid, rowIndex, colIndex, item, e, record) {
                //         console.log(record.data.id)
                //             // Ext.Msg.confirm('Confirmation', 'Are you sure?', function(btnText) {
                //             //     if (btnText === 'yes') {
                //             // Ext.Ajax.request({
                //             //     url: 'http://localhost:8080/del/' + record.data.id,
                //             //     method: 'DELETE', //this is the url where the form gets submitted
                //             //     useDefaultXhrHeader: false,
                //             //     cors: true,
                //             //     success: function(form, action) {
                //             //         store.load()
                //             //     },
                //             //     failure: function(form, action) {
                //             //         Ext.Msg.alert('Failed', action);
                //             //     }
                //             // });
                //             //     }
                //             // })
                //     }
            }],
            listeners: {
                click: 'onDeleteClick'
            }
        }
    ],
    bbar: Ext.create('Ext.PagingToolbar', {
        xtype: 'pagingtoolbar',
        displayInfo: true,
    }),

    // columns: [
    //     { text: 'Name', dataIndex: 'name', flex: 1 },
    //     { text: 'Email', dataIndex: 'email', flex: 1 },
    //     { text: 'Phone', dataIndex: 'phone', flex: 1 }
    // ],

    // listeners: {
    //     select: 'onItemSelected',
    // },
});